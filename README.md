# READ This instruction for the installation & running process
# ATL Chatbot

---
## What is this project all about?
This project is about a Chatbot, that is intended to be used by people who are seeking help in the ATL Community & want to get some answers to their questions quickly. We have also added a voice assistant which will speak everything that is written (At some point we have restricted speaking it to some things only). We have created the project in the Python programming language.

***

## Installation

### Windows
To install it on Windows you need to install `git` & `python`. You can go to the website & see how to install both of these. You need also to install `pyttsx3`. So open your command prompt & write

``` bash
pip install pyttsx3
```

**Note this will only install if you have python installed**

After this then you have to write this on your command prompt

``` bash
git clone https://gitlab.com/AnantGupta/chatbot-for-atl-2.git
```

**Note this will only work if you have git installed**

Now you have everything ready, now you can run the code!


### Linux
You need to install `python3` (Mostly installed on every distribution), `git` & `python3-pip`. You also need to install few more dependencies like `playsound` & `gtts`

#### On Ubuntu/Debian/Mint
``` bash
sudo apt-get install python3 python3-pip git
git clone https://gitlab.com/AnantGupta/chatbot-for-atl-2.git
pip install playsound gtts
```

#### On Arch/Manjaro/Arco
``` bash
sudo pacman -S --needed python python-pip git
git clone https://gitlab.com/AnantGupta/
pip install gtts playsound
```

***

## Running the code
To run the code open terminal/CMD & write

### For Windows
``` bash
cd chatbot-for-atl/
python windows.py
```

### For Linux

``` bash
cd chatbot-for-atl/
python linux.py
```

***

## Who has made it
Anant Gupta & Atharv Dixit of Bal Bharati Public School Noida has made the following code. You can contribute to the code very easily. We have made our code.
